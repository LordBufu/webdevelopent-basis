<?php

class QueryBuilder

{
    protected $pdo;
    
    public function __construct($pdo) {
        $this->pdo = $pdo;
        
    }
    
    public function selectAll($table) {
        $statement = $this->pdo->prepare("select * from {$table}");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_CLASS);
    }
    
    // My own insert method loosy based on the homework assignment.
    public function insertName($table, $name, $taskID) {
        $statement = $this->pdo->prepare("UPDATE {$table} SET assignedTo='{$name}' WHERE id='{$taskID}'");
        $statement->execute();
    }
    
    // The laracast insert method
    public function insert($table, $parameters) {
        $sql = sprintf(
            'insert into %s (%s) values (%s)',
            $table,
            implode(', ', array_keys($parameters)),
            ':' . implode(', :', array_keys($parameters))
        );
        
        try {
            $statement = $this->pdo->prepare($sql);
            $statement->execute($parameters);
        } catch (exception $e) {
            die('Whoops, something went wrong.');
        }
    }
}
?>