<?php require('partials/head.php'); ?>

<h1>Set Name to Task</h1>

<form method='POST' action='/names'>
	<h3>Input Name</h3><input name='name' type='text'></input>
	<h3>Input Task ID</h3><input name='taskID'></input>
	<input type='submit' name='updateName' value='Submit Data'/>
</form>

<br><br>
<h1>Task List & Updater</h1>

<table class="table table-striped table-condensed">
    <thead>
        <tr>
            <th>Task Number</th>
            <th>Task Description</th>
            <th>Task Assigned To</th>
            <th>Task Status</th>
        </tr>
    </thead>
    <?php foreach ($tasks as $task) : ?>
    <tbody>
        <tr>
            <td><?= $task->id; ?></td>
            <td><?= $task->description; ?></td>
            <td><?= $task->assignedTo; ?></td>
            <td><?= $task->completed; ?></td>
        </tr>
    </tbody>
    <?php endforeach; ?>
</table>
	    
<?php require('partials/footer.php'); ?>